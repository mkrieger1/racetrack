from functools import total_ordering
import numpy as np
import re
from StringIO import StringIO


@total_ordering
class Polygon(object):
    def __init__(self, vertices):
        if len(set(vertices)) < len(vertices):
            raise ValueError('The vertices must be unique.')
        starts = vertices
        ends = np.roll(starts, -1, 0)
        shape = (-1, 2, 2) # segments, start/end, x/y
        self._segments = np.reshape(np.hstack((starts, ends)), shape)

    def _cross(self, point):
        """Return the array of cross products between the segments and the
        point."""
        starts = self._segments[:, 0, :]
        ends = self._segments[:, 1, :]
        a = ends - starts
        b = point - starts
        return a[:, 0] * b[:, 1] - a[:, 1] * b[:, 0]

    @property
    def vertices(self):
        """The vertices of this polygon."""
        return self._segments[:, 0, :]

    @property
    def simple(self):
        """Whether no two segments intersect each other."""
        raise NotImplementedError

    @property
    def clockwise(self):
        """Whether the vertices are arranged in clockwise order."""
        raise NotImplementedError

    def contains(self, other):
        """Whether the other polygon is inside of this one."""
        raise NotImplementedError

    def __gt__(self, point): # poly > pt <=> pt < poly <=> pt inside poly
        """
        Whether the point is inside this polygon (excluding the boundary).

        #     K
        #   D R C
        # L S X Q J
        #   A P B
        #     I
        >>> A, B, C, D = (0, 0), (2, 0), (2, 2), (0, 2)
        >>> P, Q, R, S = (1, 0), (2, 1), (1, 2), (0, 1)
        >>> I, J, K, L = (1, -1), (3, 1), (1, 3), (-1, 1)
        >>> X = (1, 1)
        >>> box = Polygon((A, B, C, D))
        >>> X < box # in the middle
        True
        >>> [p < box for p in (I, J, K, L)] # outside
        [False, False, False, False]
        >>> [p < box for p in (A, B, C, D)] # on the corners
        [False, False, False, False]
        >>> [p < box for p in (P, Q, R, S)] # on the edges
        [False, False, False, False]
        >>> diamond = Polygon((I, J, K, L))
        >>> X < diamond # in the middle
        True
        >>> [p < diamond for p in (P, Q, R, S)] # inside
        [True, True, True, True]
        >>> [p < diamond for p in (I, J, K, L)] # on the corners
        [False, False, False, False]
        >>> [p < diamond for p in (A, B, C, D)] # on the edges
        [False, False, False, False]
        >>> outer = Polygon((P, Q, R, S))
        >>> X < outer # in the middle
        True
        >>> [p < outer for p in (P, Q, R, S)] # on the corners
        [False, False, False, False]
        >>> [p < outer for p in (I, J, K, L)] # outside
        [False, False, False, False]
        >>> [p < outer for p in (A, B, C, D)] # outside
        [False, False, False, False]
        """

        # Winding number algorithm adapted from
        # http://geomalgorithms.com/a03-_inclusion.html
        # Assumes that the point does *not* lie on the boundary!

        if point == self: # point on boundary
            return False

        is_left = self._cross(point) > 0
        starts = self._segments[:, 0, 1]
        ends = self._segments[:, 1, 1]
        y = point[1]

        up = is_left & (starts <= y) & (y < ends)
        down = ~is_left & (starts > y) & (y >= ends)

        return sum(up) != sum(down)

    def __eq__(self, point):
        """
        Whether the point lies on the boundary of this polygon.

        #     K
        #   D R C
        # L S X Q J
        #   A P B
        #     I
        >>> A, B, C, D = (0, 0), (2, 0), (2, 2), (0, 2)
        >>> P, Q, R, S = (1, 0), (2, 1), (1, 2), (0, 1)
        >>> I, J, K, L = (1, -1), (3, 1), (1, 3), (-1, 1)
        >>> X = (1, 1)
        >>> box = Polygon((A, B, C, D))
        >>> X == box # in the middle
        False
        >>> [p == box for p in (I, J, K, L)] # outside
        [False, False, False, False]
        >>> [p == box for p in (A, B, C, D)] # on the corners
        [True, True, True, True]
        >>> [p == box for p in (P, Q, R, S)] # on the edges
        [True, True, True, True]
        >>> diamond = Polygon((I, J, K, L))
        >>> X == diamond # in the middle
        False
        >>> [p == diamond for p in (P, Q, R, S)] # inside
        [False, False, False, False]
        >>> [p == diamond for p in (I, J, K, L)] # on the corners
        [True, True, True, True]
        >>> [p == diamond for p in (A, B, C, D)] # on the edges
        [True, True, True, True]
        >>> outer = Polygon((P, Q, R, S))
        >>> X == outer # in the middle
        False
        >>> [p == outer for p in (P, Q, R, S)] # on the corners
        [True, True, True, True]
        >>> [p == outer for p in (I, J, K, L)] # outside
        [False, False, False, False]
        >>> [p == outer for p in (A, B, C, D)] # outside
        [False, False, False, False]
        >>> rect = Polygon((P, B, C, R))
        >>> [p == rect for p in (D, A)] # outside on the extension of an edge
        [False, False]
        """
        collinear = self._cross(point) == 0
        # bounding boxes around each segment
        lower_left = self._segments.min(axis=1)
        upper_right = self._segments.max(axis=1)
        inside = ((point >= lower_left).all(axis=1) &
                  (point <= upper_right).all(axis=1))
        return (collinear & inside).any()

    def __contains__(self, point):
        """
        Whether the point is one of the vertices.

        #     K
        #   D R C
        # L S X Q J
        #   A P B
        #     I
        >>> A, B, C, D = (0, 0), (2, 0), (2, 2), (0, 2)
        >>> P, Q, R, S = (1, 0), (2, 1), (1, 2), (0, 1)
        >>> I, J, K, L = (1, -1), (3, 1), (1, 3), (-1, 1)
        >>> X = (1, 1)
        >>> box = Polygon((A, B, C, D))
        >>> X in box # in the middle
        False
        >>> [p in box for p in (I, J, K, L)] # outside
        [False, False, False, False]
        >>> [p in box for p in (A, B, C, D)] # on the corners
        [True, True, True, True]
        >>> [p in box for p in (P, Q, R, S)] # on the edges
        [False, False, False, False]
        >>> diamond = Polygon((I, J, K, L))
        >>> X in diamond # in the middle
        False
        >>> [p in diamond for p in (P, Q, R, S)] # inside
        [False, False, False, False]
        >>> [p in diamond for p in (I, J, K, L)] # on the corners
        [True, True, True, True]
        >>> [p in diamond for p in (A, B, C, D)] # on the edges
        [False, False, False, False]
        >>> outer = Polygon((P, Q, R, S))
        >>> X in outer # in the middle
        False
        >>> [p in outer for p in (P, Q, R, S)] # on the corners
        [True, True, True, True]
        >>> [p in outer for p in (I, J, K, L)] # outside
        [False, False, False, False]
        >>> [p in outer for p in (A, B, C, D)] # outside
        [False, False, False, False]
        """
        # implement this because "in self.vertices" has weird behaviour, see
        # http://stackoverflow.com/q/14766194/4621513
        return (point == self.vertices).all(axis=1).any()


class Track(object):
    def __init__(self, inner, outer, sf_line):
        if not inner.simple and outer.simple:
            raise ValueError('The bounds must be simple polygons.')
        if not outer.contains(inner):
            raise ValueError('The bounds must be concentric polygons.')
        if not inner.clockwise == outer.clockwise:
            raise ValueError('The bounds must have the same direction.')

        self._inner = inner
        self._outer = outer
        self._sf_line = sf_line

    @classmethod
    def from_string(cls, s):
        """Load a track from a formatted string."""
        def vertices_from_string(part):
            return np.loadtxt(StringIO(part.strip()))
        parts = map(vertices_from_string, re.split('\n\n+', s)[:3])
        return cls(*map(Polygon, parts[:2]), sf_line=parts[2])

    def to_string(self):
        """Save the track as a formatted string."""
        parts = [self._inner.vertices, self._outer.vertices, self._sf_line]
        def vertices_to_string(part):
            f = StringIO()
            np.savetxt(f, part)
            return f.getvalue()
        return '\n\n'.join(map(vertices_to_string, parts))

